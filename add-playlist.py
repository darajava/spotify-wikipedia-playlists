import wikitextparser as wtp
import time
import requests
import json
import spotipy.util as util
import spotipy
import re
import spotipy.oauth2 as oauth2
import os

token = util.prompt_for_user_token(os.environ['SPOTIFY_USER'], scope='playlist-modify-private,playlist-modify-public', client_id=os.environ['SPOTIFY_CLIENT_ID'], client_secret=os.environ['SPOTIFY_CLIENT_SECRET'], redirect_uri='https://localhost:8080')

spotify = spotipy.Spotify(auth=token)

def getSpotifyUrl(searchTerm):
  try:
    results = spotify.search(q=searchTerm, type='track')
    time.sleep(0.5);
  except Exception as e:
    print(e);
    print('sleeping')
    time.sleep(10);
    return getSpotifyUrl(searchTerm);

  try:
    return results['tracks']['items'][0]['external_urls']['spotify'];
  except:
    return None 
def getTrackList(title):
  r = requests.get('http://en.wikipedia.org/w/index.php?title='
    + title + '&action=raw')

  result = [];  

  wt = wtp.parse(r.text)
  for entry in wt.tables[0].data()[1:]:
    # if (len(result) >= 30):
      # return result;

    # print(entry[1], entry[2])
    current = re.sub(r'.*\|', '', entry[1]) + ' ' + re.sub(r'.*\|', '', entry[2])
    searchquery = current.replace('[', '').replace(']','').replace('"', '')
    searchquery = re.sub(r'<[^<]+?>', '', searchquery);
    searchquery = re.sub(r'\bwith\b', '', searchquery);
    searchquery = re.sub(r'\band\b', '', searchquery);
    searchquery = re.sub(r'\bfeaturing\b', '', searchquery);
    searchquery = re.sub(r'\bft\b', '', searchquery);
    print(searchquery)

    url = getSpotifyUrl(searchquery)

    if url is not None:
      result.append(url)

  return result;

def createPlaylist(year):
  try:
    return [
      spotify.user_playlist_create("1194469055", "Billboard " + year + " top 10 year-end singles", public=True)['id'],
      spotify.user_playlist_create("1194469055", "Billboard " + year + " top 30 year-end singles", public=True)['id'],
      spotify.user_playlist_create("1194469055", "Billboard " + year + " top 100 year-end singles", public=True)['id'],
    ];
  except Exception as e:
    print(e)
    print('sleeping')
    time.sleep(10);
    return createPlaylist(year);


def addToPlaylist(tracks, playlist_id):
  try:
    spotify.user_playlist_add_tracks("1194469055", playlist_id, tracks, position=None)
  except Exception as e:
    print(e)
    print('sleeping')
    time.sleep(10);
    return addToPlaylist(tracks, playlist_id);

#open('test.txt', 'w').close()

for year in range(2005, 2019):
  print(year)

  prefix = "Billboard_Year-End_Hot_100_singles_of_";
  if (year >= 1950 and year <= 1955):
    prefix = "Billboard_year-end_top_30_singles_of_";
  elif (year >= 1956 and year <= 1958):
    prefix = "Billboard_year-end_top_50_singles_of_"; 

  playlist = createPlaylist(str(year))
  with open("test.txt", "a") as myfile:
    myfile.write('### ' + str(year) + "\n")
    myfile.write('Spotify\n');
    myfile.write('<sup><a href="https://open.spotify.com/playlist/' + playlist[0] + '">Top 10</a></sup> ');
    myfile.write('<sup><a href="https://open.spotify.com/playlist/' + playlist[1] + '">Top 30</a></sup> ');
    myfile.write('<sup><a href="https://open.spotify.com/playlist/' + playlist[2] + '">Top 100</a></sup>\n\n');
    myfile.write('Wikipedia\n');
    myfile.write('<sup>[Tracklist](https://en.wikipedia.org/wiki/' + prefix + str(year) + ")</sup>\n\n");
  
  tracks = getTrackList(prefix + str(year))
  addToPlaylist(tracks[:10], playlist[0]);
  addToPlaylist(tracks[:30], playlist[1]);
  addToPlaylist(tracks[:99], playlist[2]);

