import wikitextparser as wtp
import time
import requests
import json
import spotipy.util as util
import spotipy
import re
import spotipy.oauth2 as oauth2
import os

print (os.environ['SPOTIFY_CLIENT_SECRET'])

token = util.prompt_for_user_token(os.environ['SPOTIFY_USER'], scope='playlist-modify-private,playlist-modify-public', client_id=os.environ['SPOTIFY_CLIENT_ID'], client_secret=os.environ['SPOTIFY_CLIENT_SECRET'], redirect_uri='https://localhost:1500')

spotify = spotipy.Spotify(auth=token)

def makePlaylistPrivate(playlist_id):
  try:
    print(playlist_id)
    return spotify.user_playlist_change_details("1194469055", playlist_id=playlist_id, public=False)
  except Exception as e:
    print(e)
    print('sleeping')
    time.sleep(10);
    return makePlaylistPrivate(playlist_id);


def getPlaylists(offset):
  try:
    return spotify.user_playlists("1194469055", offset=(offset*50), limit=50)
  except Exception as e:
    print(e)
    time.sleep(10);
    return getPlaylists();


i = 0
while i == 0 or len(playlists['items']) > 0: 
  playlists = getPlaylists(i);
  i = i + 1;

  print(json.dumps(playlists, indent=4, sort_keys=True))

  for playlist in playlists['items']:
    print(playlist['name']);
    if ("#1" in playlist['name']):
      makePlaylistPrivate(playlist['id']);

