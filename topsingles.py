import wikitextparser as wtp
import time
import requests
import json
import spotipy.util as util
import spotipy
import re
import spotipy.oauth2 as oauth2
import os

token = util.prompt_for_user_token(os.environ['SPOTIFY_USER'], scope='playlist-modify-private,playlist-modify-public', client_id=os.environ['SPOTIFY_CLIENT_ID'], client_secret=os.environ['SPOTIFY_CLIENT_SECRET'], redirect_uri='https://localhost:8080')

spotify = spotipy.Spotify(auth=token)

def getSpotifyUrl(searchTerm):
  try:
    results = spotify.search(q=searchTerm, type='track')
    time.sleep(0.5);
  except Exception as e:
    print(e);
    print('sleeping1')
    time.sleep(10);
    return getSpotifyUrl(searchTerm);

  try:
    return results['tracks']['items'][0]['external_urls']['spotify'];
  except:
    return None 

def getTrackList(column):
  r = requests.get('http://en.wikipedia.org/w/index.php?title='
    + 'List_of_Billboard_Year-End_number-one_singles_and_albums' + '&action=raw')

  result = [];

  wt = wtp.parse(r.text)
  for entry in wt.tables[0].data()[2:]:
    # if (len(result) >= 30):
      # return result;

    entry[column] = re.sub(r'<.*>', '', entry[column]);
    entry[column] = re.sub(r'{{[^}]*}}', '', entry[column]);
    parts = re.findall(r'\[\[[^\]]*\]\]', entry[column])

    for part in parts:
      entry[column] = entry[column].replace(part, "")

    parts.append(entry[column]);

    # print(entry[column]);
    final_result = "";
    for part in parts:
      # print(part)
      # print(entry[1], entry[2])
      searchquery = re.sub(r'.*\|', '', part)
      searchquery = searchquery.replace('[', '').replace(']','').replace('"', '')
      searchquery = re.sub(r'<[^<]+?>', '', searchquery);
      searchquery = re.sub(r'\bwith\b', '', searchquery);
      searchquery = re.sub(r'\band\b', '', searchquery);
      searchquery = re.sub(r'\bfeaturing\b', '', searchquery);
      searchquery = re.sub(r'\bft\b', '', searchquery);
      
      final_result = final_result + " " + searchquery

    print(final_result)
    url = getSpotifyUrl(final_result);

    # print(entry[0])
    if url is not None:
      result.append(url)

  return result;

def createPlaylists():
  try:
    return [
      spotify.user_playlist_create("1194469055", "Billboard Pop #1 singles 1946 - 2018", public=True)['id'],
      spotify.user_playlist_create("1194469055", "Billboard R&B/Soul/Hip-hop #1 singles 1946 - 2018", public=True)['id'],
      spotify.user_playlist_create("1194469055", "Billboard Country #1 singles 1946 - 2018", public=True)['id'],
    ];
  except Exception as e:
    print(e)
    print('sleeping2')
    time.sleep(10);
    return createPlaylist(year);


def addToPlaylist(tracks, playlist_id):
  try:
    spotify.user_playlist_add_tracks("1194469055", playlist_id, tracks, position=None)
  except Exception as e:
    print(e)
    print('sleeping3')
    time.sleep(10);
    return addToPlaylist(tracks, playlist_id);

#open('test.txt', 'w').close()

for year in range(2005, 2019):
  print(year)

prefix = "https://en.wikipedia.org/wiki/List_of_Billboard_Year-End_number-one_singles_and_albums"

playlist = createPlaylists()
with open("test.txt", "a") as myfile:
  myfile.write('### Billboard #1 1946 - 2018' + "\n")
  myfile.write('Spotify\n');
  myfile.write('<sup><a href="https://open.spotify.com/playlist/' + playlist[0] + '">Pop</a></sup> ');
  myfile.write('<sup><a href="https://open.spotify.com/playlist/' + playlist[1] + '">R&B/Soul/Hip-hop</a></sup> ');
  myfile.write('<sup><a href="https://open.spotify.com/playlist/' + playlist[2] + '">Country</a></sup>\n\n');
  myfile.write('Wikipedia\n');
  myfile.write('<sup>[Tracklists](' + prefix + ")</sup>\n\n");

addToPlaylist(getTrackList(1), playlist[0]);
addToPlaylist(getTrackList(3), playlist[1]);
addToPlaylist(getTrackList(5), playlist[2]);
# getTrackList(3)

