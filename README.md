Outputs a file similar to:

### Billboard #1 Singles 1946 - 2018
**Spotify**

<a href="https://open.spotify.com/playlist/5IDHB5YDPvzjWWokP1mESY">Pop</a>

<a href="https://open.spotify.com/playlist/5Oz1aBVii5BJdNaRvwyx95">R&B/Soul/Hip-hop</a>

<a href="https://open.spotify.com/playlist/2btjLo2lU0M8tiBWwoeCCq">Country</a>

**Wikipedia**

<sup>[Tracklists](https://en.wikipedia.org/wiki/List_of_Billboard_Year-End_number-one_singles_and_albums)</sup>

## Top playlists by year

### 1950
**Spotify**

<sup><a href="https://open.spotify.com/playlist/5sQbA6oKrOwgXxVPYucUGu">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/0Z1WYISJtd52GLeO21eIxR">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4uASff54YBwHwvt06wOwci">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_30_singles_of_1950)</sup>

### 1951
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1oIuu9WN1fNmnmntcNlNa2">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/0asVks0we0A7Sg1Ubez0Yy">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/40jJaGjGom3smu66gja114">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_30_singles_of_1951)</sup>

### 1952
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6kKgUzc07dFTQoZHAjkBkt">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/4WgsM7DjmrB8bKMPGEUqMY">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/3zkK6ASvAPqyqwTw4GR1KM">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_30_singles_of_1952)</sup>

### 1953
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1Po5gUXa5kYbKc4WS9TxuZ">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7cbHBjHyZsGnYG5ijgWipc">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/0InYYhOMN2ohxXdmwDxMk1">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_30_singles_of_1953)</sup>

### 1954
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7dbOPwggv97eYxGmjxsuOS">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/33EMJCrUAW7jTiTktX8T0K">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1dLguc3irhXBZ1POU7s2WX">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_30_singles_of_1954)</sup>

### 1955
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0sfeC4my8REGp89JME8IDL">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/44u0S4OaZNLHqFN7ryXisc">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/6kzOs0SeF4aX5T0cUhFQGt">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_30_singles_of_1955)</sup>

### 1956
**Spotify**

<sup><a href="https://open.spotify.com/playlist/3zEjBRViwrKgejAoXupDZV">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/1yMpwfbW06462QFc3nKJck">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/0A2U1JEoT42GGyfGTaJ8Iy">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_50_singles_of_1956)</sup>

### 1957
**Spotify**

<sup><a href="https://open.spotify.com/playlist/3Lj6hTDCiyGYSsmONpCQHp">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2NEs1r3xcDxvfR5mvSCtLS">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7BNUBFMZr8pfCSIW431UrP">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_50_singles_of_1957)</sup>

### 1958
**Spotify**

<sup><a href="https://open.spotify.com/playlist/39uOOJZDv0gWaRZkGxk41W">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2oxX11O0JeeTtvKLGLZrRL">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1GM73S0y9AslCFsmeq3jWo">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_year-end_top_50_singles_of_1958)</sup>

### 1959
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6aYrIdGXWuJsl7sThOc9SE">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3VLfCn8sd0ngjp5WhYzpg3">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/6dt66gvMT6tQnNo6cISgEJ">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1959)</sup>

### 1960
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7z9vUdsG2jgGQXft2p9URg">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/1rJXrRBCUQcgvtMSNUUY5B">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/219TqcglFeJ3asDIFnNuTA">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1960)</sup>

### 1961
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7j9kQJP5QkaRnEGfIrnxyE">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/5qjgUn95zd3MbdX4eCGhjp">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4A1bH9Kc1RIfzeIgLTHIYQ">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1961)</sup>

### 1962
**Spotify**

<sup><a href="https://open.spotify.com/playlist/29nD9FZejBByIpxhcOIx2i">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7D18wt5Sg0oj0hYonlnjDy">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/29x3hyk7WEAgAIyBlgWJAo">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1962)</sup>

### 1963
**Spotify**

<sup><a href="https://open.spotify.com/playlist/5qqyWl9QlJEUztSSGfe7VT">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3epE9tGiWmP87G6fw3wnBp">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7szZ90RDiDW9MYyqbY0hXH">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1963)</sup>

### 1964
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0TCoRY2AHUUM0mnOjpabHv">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3RzV1g9ezvVyDz61K6EF6w">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/0ASgPCSGiu6Ej7Zv53Tlff">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1964)</sup>

### 1965
**Spotify**

<sup><a href="https://open.spotify.com/playlist/2xyfaQuIzh0XFULrOdRJBN">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3itEWPbGLWCow7ilWtZw33">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1lOvAaPVLiFe7sQfGUyULX">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1965)</sup>

### 1966
**Spotify**

<sup><a href="https://open.spotify.com/playlist/3IE2gJVptjUzzxC81VPywD">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2C2R0gDzuySWaKBO5G45WH">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/6l8174Qa5iS6ebNE4O6Yhi">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1966)</sup>

### 1967
**Spotify**

<sup><a href="https://open.spotify.com/playlist/2UjYy3QxbEAlfUIcgtFK6e">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/32Y2ubCCNmGy74dc6Qvk8T">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/0fcasSBs8qAsrlgI2L8UKU">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1967)</sup>

### 1968
**Spotify**

<sup><a href="https://open.spotify.com/playlist/62eeN0WHI7jeLSBWGpyXVT">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7xx37oV7JJPvE0NkI9etxK">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7FLi11Z7R6b2NHwJV7KxeZ">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1968)</sup>

### 1969
**Spotify**

<sup><a href="https://open.spotify.com/playlist/4G0IUU6cPy1A4Cn68y4f5t">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/1ADi4jVd9sr6xIHErAXGkh">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1KBnumo9B5ctlpk2gVYQQh">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1969)</sup>

### 1970
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6RBPBpcpWNsYbRdHQnPik5">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/4Qluy0jCUtxPsTV2TGoxSo">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5IYv9GnzOoOw7KLz3zcjvh">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1970)</sup>

### 1971
**Spotify**

<sup><a href="https://open.spotify.com/playlist/59ulF9QZ8L8pifPqfM1zeV">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/5xs5D9WzGJ3ANLKqmePDee">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/2zlaKxTFWN8cUvhwMsmiYu">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1971)</sup>

### 1972
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0Y6zIDON3XOiqgaXp8czsy">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/4dRe5PNW1VN6WtcPIcr0Fm">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1jtEIYItmJ0nfDEq2Xq28P">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1972)</sup>

### 1973
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6FN3byKHFdSikuRWmb2geZ">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6sFYNq1cyedsVBi9TJJT9h">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1DUUEtPxmTwNOLsYpgEsbR">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1973)</sup>

### 1974
**Spotify**

<sup><a href="https://open.spotify.com/playlist/12OqSb7XK15y8nkXdKJnPf">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/1dFPnVMMGi2o0OgqqJB5ri">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/2ZCScMYufW6NNLRoHWUA3E">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1974)</sup>

### 1975
**Spotify**

<sup><a href="https://open.spotify.com/playlist/78EEVOFgNAjeZZUQ3u2R2t">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/173LjStvVHF1jUw9liWXDh">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4ARpO6tiz3CtgwJvSZfSyQ">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1975)</sup>

### 1976
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0NsjeGKGC5W01Nbh5Hwhpd">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/1SyFfKwA5VUMh4K475pBAi">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5EjKL1dSd4KIN0YsqAQEVa">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1976)</sup>

### 1977
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1CXbMEISR48OMGqbeujMNn">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6UrYw16ugvI1FccW1VVuf7">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/538qRYRtxI6qHcEBuOzOuZ">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1977)</sup>

### 1978
**Spotify**

<sup><a href="https://open.spotify.com/playlist/4QDJu2MXyGajuKd8rUfJYF">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2UXb5XatneC5yKRz5wN7DC">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/70VX9TE41BaP7SDXRKZ6gX">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1978)</sup>

### 1979
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7bwoVGXmgIMZOm1j7zHtv4">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7sk7ax260szjTviy7hgb1R">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/6M3DKZKEPZW7UA1d6pZkDJ">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1979)</sup>

### 1980
**Spotify**

<sup><a href="https://open.spotify.com/playlist/5RON7u9qwkOVlsllrYAOld">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6w3x1Vm4655bzvH2YI8xiU">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5z2i8DpG77TOkQl7IyjIgU">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1980)</sup>

### 1981
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1iMOGpQS0be7tHtnxw2smY">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/4a4F9Ug0IFmBwu2wSsecpR">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5Ut7z3i6ZaCgNsvlc3rfVN">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1981)</sup>

### 1982
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0mMZ1FfSvrv5rh7QIT4j4l">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6B1JlwI92dpNdKpuoEuAI0">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/16cjAzVX07FvF1ATwKKXQD">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1982)</sup>

### 1983
**Spotify**

<sup><a href="https://open.spotify.com/playlist/3iTRqsOKTpy8QwEtNj7c39">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/0c1WUGyyERaJimMtHYZrUl">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5JzhmlEyFbnVGPYZo2VuqH">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1983)</sup>

### 1984
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6QMGDdk2fbY4wih2zOiQGq">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7kdm4IAOvxiy2o6fSveSjW">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1x4JrTIGFus4wYKuPODGIC">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1984)</sup>

### 1985
**Spotify**

<sup><a href="https://open.spotify.com/playlist/4htiqoiIbk5bDo8I9BlPtg">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2XbssYWFyBn0zYZmyTKIHw">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/35ISTs2hQHjJ75rPqXEmPf">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1985)</sup>

### 1986
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0lZlXpnECMb1SG1W4nOu8w">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7fkFuycgJUpjb8oteC1V64">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/0ALzJw0POHd8xsekk2PBLv">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1986)</sup>

### 1987
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1E6aGiGEgyjrAtGNH32KLf">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/4UoVQdVd8jhgauRB6ktyfi">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5equb1ZKq2RsHaZSZorHJ1">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1987)</sup>

### 1988
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7GXh7JpvdN9TF0Qy8i4qqw">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3YtP7ek2eHQCtbcORAYC6c">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/2wvzti5awnzl64btUZ0foj">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1988)</sup>

### 1989
**Spotify**

<sup><a href="https://open.spotify.com/playlist/2gt7Ru7j8KhkWDMCcydUid">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/36IKgLPHkzyK4To4EmUljX">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5xApQ1VEL5U8onKPStt53X">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1989)</sup>

### 1990
**Spotify**

<sup><a href="https://open.spotify.com/playlist/2TSv6CUalx3O3bBK7FihLZ">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/5tlkfteZM8mSebF3p9QefC">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/2GeigjjMDyKzLXb3EZVv0x">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1990)</sup>

### 1991
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0iNRarhWf0VA9CY9JA2TG0">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2A0NADVlZMZaJbMM3xlmSk">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/2ID4oPBPNLLnhWqsu7Te1l">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1991)</sup>

### 1992
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1py1ac27wg5eEU6PdNtwwA">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7aALMnIj5EOwMcxXrDYwmu">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/2IVQ9CInpB0N8z1c9C2Jka">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1992)</sup>

### 1993
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1M2PmbwVEyiMrESZ4sVNoG">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7cRH5WKFFFgoTcPOGeZ20E">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5ZEqBOHW06QWtezrcVFjTT">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1993)</sup>

### 1994
**Spotify**

<sup><a href="https://open.spotify.com/playlist/5RZLXz31F6WOlBYwilI7EP">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2cXcyc6DvmNSwWayJYbANM">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1ZxgRFELD5oBPX6xTujsIA">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1994)</sup>

### 1995
**Spotify**

<sup><a href="https://open.spotify.com/playlist/3Yhz3EKAns6DqJQFr4qkOp">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/4YaQDIAAufBp9EdZUo1Zkm">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/00Ao2tv9TCmG7T7lZfIHg6">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1995)</sup>

### 1996
**Spotify**

<sup><a href="https://open.spotify.com/playlist/5hUkMwES09acs6ElXDoTyo">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3f0VyOgLyUhaLn7Hw7YWSf">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4a0gBPcR3c8EHsLFEo8Qgx">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1996)</sup>

### 1997
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0wWEPa5tLSgjuHcSUYcc06">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/0uhNm7JpaRxNHUirvL5GW6">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/6ritrxo7x2gl6eRFXaH7e6">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1997)</sup>

### 1998
**Spotify**

<sup><a href="https://open.spotify.com/playlist/0Hv7dSJlBRS5NpEtkw0A7l">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3IsHeUqiNLlt90KDlyKtTm">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/6d8GXlBMyajHwSYoFWIMwH">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1998)</sup>

### 1999
**Spotify**

<sup><a href="https://open.spotify.com/playlist/295KxPMNGbNK9kNx2oeYE5">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/118DfAy0HVFT9sHSzF3MPk">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1czc5V2DgoHFsClEEsqg0U">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1999)</sup>

### 2000
**Spotify**

<sup><a href="https://open.spotify.com/playlist/3HKCbpR9c53DGwmnd4fgs2">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/4KyZOXZIRuUn8miA6EdcNF">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4CAbzWjMtEomveUluwbsAU">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2000)</sup>

### 2001
**Spotify**

<sup><a href="https://open.spotify.com/playlist/19ThBdhGQuNp6wTjYwiLJS">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7mXRVk4vFHv7JtJhJXLorL">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7GX0tcPkIPMuez7rLCIsPv">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2001)</sup>

### 2002
**Spotify**

<sup><a href="https://open.spotify.com/playlist/15G5mQRzBrmCK8VKE6JKPL">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3prWo4LMmlPIWrfk6Usyh3">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/3CaciBANjRXWdaHiEWMiTw">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2002)</sup>

### 2003
**Spotify**

<sup><a href="https://open.spotify.com/playlist/2jMhunqQdTHlanQNoHwydN">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/0hMC20vsIfKIWNu2od9BWg">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/0FIZmKR61MLYBlXlVbVPpL">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2003)</sup>

### 2004
**Spotify**

<sup><a href="https://open.spotify.com/playlist/5U6OO0kOqpl8El9sWcfBnp">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6D4zqUPLpgNUVmltByK6rF">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7JDmsyrspnAK0kBDBM5Hj7">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2004)</sup>

### 2005
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6mKlwJQTTOZojFhsEObyAx">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/7L89t6cZN5N7NH7ZzOcEJ3">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/6A5NKAdTR7cJzX3UjgNOpq">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2005)</sup>

### 2006
**Spotify**

<sup><a href="https://open.spotify.com/playlist/4lgT0o4wHVMM2Dy6b34kk0">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6JwvfZuiTgnFcFripXqYC5">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7pDDyIFiWK2rf2bTsxAw5C">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2006)</sup>

### 2007
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6aqBjQ7Bgx78T0S9nVhUfK">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/0qGM4xkqwRXDnEWxP8kTTV">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7IkB8vbnibcsbW7vil0tNF">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2007)</sup>

### 2008
**Spotify**

<sup><a href="https://open.spotify.com/playlist/4LcL4HtTMGDXBjD0Znmr97">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/5x1TSs2yHGWOnywOHIVREt">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4qWkqanDP4Y0ur2db4dm4S">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2008)</sup>

### 2009
**Spotify**

<sup><a href="https://open.spotify.com/playlist/07TGudlAkIcOf1w20jQL1M">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/0e0TbDVI1xsHucvEfurceA">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/30BWp9U0ll4fePrTQV0fp8">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2009)</sup>

### 2010
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1E906NCEGkB9OypXMwIZXo">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/5MLcWuVrHNQHbNJT2vA0fo">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5aVGxFa2WNtKMPqatT9X50">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2010)</sup>

### 2011
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7aexqQ18yaXqqZyBK36Nu9">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/3CZIC3viU0jipCiiugIGeJ">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4O7aFu9Vqx5B6uZ15kK2SS">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2011)</sup>

### 2012
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7gM2LaBHPAmy1etj8wlX20">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/5FonGWsfpIQyLHAHxYON0j">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1syup6eWypibgso1DpX7q7">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2012)</sup>

### 2013
**Spotify**

<sup><a href="https://open.spotify.com/playlist/4MKp8aY4vDe88dfehYp3jb">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6qJJlBD0T3FCIuJxRFYmwx">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/4q8H7C8q8HvCNUCorGzdN3">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2013)</sup>

### 2014
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6zMeHlTFY09e4sCEGmpuo6">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/6FOstLd4HZ0oDhnSZPPmBu">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/1wUdxRQpEHfd8GTfziWZVN">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2014)</sup>

### 2015
**Spotify**

<sup><a href="https://open.spotify.com/playlist/4rHObgt9IwAB3i7Zeq7RYp">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/5VCjsYLw9z9wRfUsGYtWX0">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7x1bttCcjUYzIfBa7WYcqb">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2015)</sup>

### 2016
**Spotify**

<sup><a href="https://open.spotify.com/playlist/6HeVO5sTN9CpGDfHRiKU9V">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2Ma1kRpVrzQOs2s7pvkh0z">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5detmb6zKaYwLAb5rDpsXA">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2016)</sup>

### 2017
**Spotify**

<sup><a href="https://open.spotify.com/playlist/7wjfKhI7NpMcKRUwVxmxxi">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/2ZrporgLkRjSGM9mb18VWY">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/7wqXBGB6QU923ompNnsseg">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2017)</sup>

### 2018
**Spotify**

<sup><a href="https://open.spotify.com/playlist/1E4c0Ate3Gx4srNSPOxaXk">Top 10</a></sup> <sup><a href="https://open.spotify.com/playlist/23OpCLXA5Vr5uUAiyivuB8">Top 30</a></sup> <sup><a href="https://open.spotify.com/playlist/5kaG04KcZ0gcJE0ndSVwVq">Top 100</a></sup>

**Wikipedia**

<sup>[Tracklist](https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_2018)</sup>